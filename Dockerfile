#FROM node:13
FROM node:12-slim

#RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

#WORKDIR /home/node/app

#COPY package*.json ./


#RUN npm install -g nodemon

#COPY --chown=node:node . .
RUN  npm install -g peer

EXPOSE 3000

#USER node
#CMD [ "npm","peer","npm","start" ]
CMD [ "bash", "start.sh" ]
#CMD [ "npm", "start" ]